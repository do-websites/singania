$(document).ready(function() {
    //Element one hover

    $("a#svimg1").hover(
        function(e) {
            $("a.clb, a.clc, a.cld, a.cle, a.clf, a.clg, a.clh").addClass("op");
            $("p.rooms_text_one#link_blog").addClass("link_vb");
        },

        function(e) {
            $("p.rooms_text_one#link_blog").removeClass("link_vb");
            $("a.clb, a.clc, a.cld, a.cle, a.clf, a.clg, a.clh").removeClass("op");
        }
    );

    //Element two hover
    $("a#svimg2").hover(
        function(e) {
            $("a.cla, a.clc, a.cld, a.cle, a.clf, a.clg, a.clh").addClass("op");
            $("p.rooms_text_one#link_pm").addClass("link_vb");
        },
        function(e) {
            $("p.rooms_text_one#link_pm").removeClass("link_vb");
            $("a.cla, a.clc, a.cld, a.cle, a.clf, a.clg, a.clh").removeClass("op");
        }
    );

    //Element four hover
    $("a#svimg4").hover(
        function(e) {
            $("a.cla, a.clb, a.clc, a.cle, a.clf, a.clg, a.clh").addClass("op");
            $("p.rooms_text_one#link_abt").addClass("link_vb");
        },
        function(e) {
            $("p.rooms_text_one#link_abt").removeClass("link_vb");
            $("a.cla, a.clb, a.clc, a.cle, a.clf, a.clg, a.clh").removeClass("op");
        }
    );

    //Element five hover
    $("a#svimg5").hover(
        function(e) {
            $("a.cla, a.clb, a.clc, a.cld, a.clf, a.clg, a.clh").addClass("op");
            $("p.rooms_text_one#link_contact").addClass("link_vb");
        },
        function(e) {
            $("p.rooms_text_one#link_contact").removeClass("link_vb");
            $("a.cla, a.clb, a.clc, a.cld, a.clf, a.clg, a.clh").removeClass("op");
        }
    );

    //Element six hover
    $("a#svimg6").hover(
        function(e) {
            $("a.cla, a.clb, a.clc, a.cld, a.cle, a.clg, a.clh").addClass("op");
            $("p.rooms_text_one#link_services").addClass("link_vb");
        },
        function(e) {
            $("p.rooms_text_one#link_services").removeClass("link_vb");
            $("a.cla, a.clb, a.clc, a.cld, a.cle, a.clg, a.clh").removeClass("op");
        }
    );

    //Element seven hover
    $("a#svimg7").hover(
        function(e) {
            $("a.cla, a.clb, a.clc, a.cld, a.cle, a.clf, a.clh").addClass("op");
            $("p.rooms_text_one#link_clients").addClass("link_vb");
        },
        function(e) {
            $("p.rooms_text_one#link_clients").removeClass("link_vb");
            $("a.cla, a.clb, a.clc, a.cld, a.cle, a.clf, a.clh").removeClass("op");
        }
    );

    //Element eight hover
    $("a#svimg8").hover(
        function(e) {
            $("a.cla, a.clb, a.clc, a.cld, a.cle, a.clf, a.clg").addClass("op");
            $("p.rooms_text_one#link_pl").addClass("link_vb");
        },
        function(e) {
            $("p.rooms_text_one#link_pl").removeClass("link_vb");
            $("a.cla, a.clb, a.clc, a.cld, a.cle, a.clf, a.clg").removeClass("op");
        }
    );

    //Element three hover
    $("a#svimg3").hover(
        function(e) {
            $("a.cla, a.clb, a.cld, a.cle, a.clf, a.clg, a.clh").addClass("op");
            $("p.rooms_text_one#link_test").addClass("link_vb");
        },
        function(e) {
            $("p.rooms_text_one#link_test").removeClass("link_vb");
            $("a.cla, a.clb, a.cld, a.cle, a.clf, a.clg, a.clh").addClass("op");
        }
    );

    $("#box_tmp").hover(function(e) {
        $("a.cla, a.clb, a.clc, a.cld, a.cle, a.clf, a.clg, a.clh").removeClass(
            "op"
        );
    });
    $("#wrp_rm .rooms_text").hover(function(e) {
        $("a.cla, a.clb, a.clc, a.cld, a.cle, a.clf, a.clg, a.clh").removeClass(
            "op"
        );
    });
});

$(document).ready(function() {
    $("#testimonial-slider").owlCarousel({
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [768, 1],
        pagination: false,
        navigation: true,
        navigationText: ["", ""],
        autoPlay: true,
    });

    $("#owl-demo").owlCarousel({
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [768, 1],
        autoPlay: true,
        navigation: false, // Show next and prev buttons
        pagination: true,
        slideSpeed: 1000,
        paginationSpeed: 1000,
        singleItem: true,

        // "singleItem:true" is a shortcut for:
        // items : 1,
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false
    });

    //contact section
    $(function() {
        var formAnimatedInput = $(".form-animate-fields .form-input");

        formAnimatedInput.each(function() {
            var $this = $(this);

            $this.on("focus", function() {
                $this.addClass("is-filled");
            });

            $this.on("blur", function() {
                if ($this.val().length == 0) {
                    $this.removeClass("is-filled");
                }
            });
        });
    });

    $(".SlickCarousel").slick({
        autoplay: true,
        autoplaySpeed: 5000, //  Slide Delay
        speed: 800, // Transition Speed
        slidesToShow: 1, // Number Of Carousel
        slidesToScroll: 1, // Slide To Move
        pauseOnHover: false,
        easing: "linear",
        responsive: [{
                breakpoint: 801,
                settings: {
                    slidesToShow: 1,
                },
            },
            {
                breakpoint: 641,
                settings: {
                    slidesToShow: 1,
                },
            },
            {
                breakpoint: 481,
                settings: {
                    slidesToShow: 1,
                },
            },
        ],
    });
});