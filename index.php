<!DOCTYPE html>
<html lang="en">

<!-- Head Section Started -->

<head>
    <meta charset="UTF-8">
    <meta name="title" content="singania | singania">
    <meta name="description" content="singania">
    <meta name="theme-color" content="#4452a3">
    <title>Home| Singania</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <link rel="shortcut icon" href="images/logo.png" type="image/png">
    <link rel="icon" href="images/logo.png" type="image/png">
    <link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
    <link rel="icon" href="images/logo.png" type="image/x-icon">

    <!-- Og M -->
    <meta property="og:type" content="website" />
    <meta property="og:title" content="singania | singania" />
    <meta property="og:description" content="singania" />
    <meta property="og:url" content="index.html" />
    <meta property="og:site_name" content="singania | singania" />
    <meta property="og:image" content="dist/img/static/og_image.html" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="singania" />
    <meta name="twitter:title" content="singania | singania" />
    <meta name="twitter:image" content="wp-content/themes/animat/dist/img/static/og_image.png" />

    <link rel="stylesheet" href="wp-content/themes/animat/owl.carousel.min.css">
    <link rel="stylesheet" href="wp-content/themes/animat/jquery.modal.min.css" />
    <link rel="stylesheet" type="text/css" href="wp-content/themes/animat/slick.min.css" />
    <link rel="stylesheet" type="text/css" href="wp-content/themes/animat/slick-theme.min.css" />

    <link rel="stylesheet" type="text/css" href="wp-content/themes/animat/styleca8b.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="all_style.css">


</head>
<!-- -----------------------Head section End------------------- -->


<body data-popup="1">

    <!-- Preloader -->
    <img id="animLogo" class="circle pulsepre" src="images/preloaderl.jpg">
    <div class="screenloader" style="height:100vh; position: fixed; width:100vw; top:0; left:0; z-index:1;">
        <div class="introAnim animA"></div>
        <div class="introAnim animB"></div>
        <div class="introAnim animC"></div>
        <div class="introAnim animD"></div>
    </div>
    <!-- Preloader -->

    <div class="modal-background"></div>
    <audio id="myAudio">
        <source src="images/audio.mp3" type="audio/ogg">
        <source src="images/audio.mp3" type="audio/mpeg">
    </audio>
    <script>
    var x = document.getElementById("myAudio");

    function playAudio() {
        x.play();
    }

    function pauseAudio() {
        x.pause();
    }
    </script>


    <!-- Whatsapp Button -->
    <!--  <div id="awwwards" style="position: fixed; z-index: 999; transform: translateY(-50%); top: 50%;  right: 0; width: 55px;">
        <a href="https://api.whatsapp.com/send?phone=9876543210&text=singhania" target="_blank" style="float:right;">
            <img src="images/whatsapp.png" style="width:50px;" /></a>
    </div> -->
    <!-- whatsapp -->



    <!-- Header Section starts -->
    <div class="mobi-head-strip"></div>
    <header class="site--header" id="header">

        <!-- logo -->
        <a href="#" class="logo flex flex--hcenter logo_adjust" id="start" onclick="playAudio()">
            <img src="images/logo-1.png" />
        </a>

        <div class="hamburger" id="hamburger" onclick="playAudio()">
            <span class="line"></span>
            <span class="line"></span>
            <span class="line"></span>
        </div>
    </header>
    <!-- Header Section ends -->

    <!-- mouse drag bolb -->
    <section class="section section--fixed section--full" id="jsFixed">
        <div class="section__wrapper flex flex--vcenter flex--hcenter flex--column">
            <br><br>
            <div class="blobmain">
                <div class="blob__container">
                    <div class="blob blob--big" id="start_pos">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="btncont" id="text_2">
                <a href="#" class="btn jsEnter" id="enter" title="Explore" rel="nofollow">Explore</a>
            </div>
        </div>
    </section>
    <div class="splash_ie"></div>
    <!-- mouse drag bolb end -->



    <!-- Home page SVG Section -->
    <section class="hmsv section section--fixed section--full rooms" id="jsRooms">

        <!-- svg menu -->
        <?php require_once('svg.php'); ?>
    </section>
    <!-- // Home page SVG Section -->




    <!-- All the page sections -->
    <div class="section section--full hidden" id="scenes">
        <div class="section__wrapper">

            <!-- Services Section -->
            <div class="scene section_Services" data-id="Services">
                <div class="scene__inner flex flex--hcenter flex--vcenter">
                    <div class="half"><br /><br />
                        <h1 class="left_border">What we do</h1>
                        <img src="images/1111s.gif" style="width:90%;" class="only-mobile" />
                        <div class="text text--small">
                            <p style="color:black;">Singhania Offset Printers offers unique, custom services to our
                                customers using the latest technology, creativity, expertise and experience for complete
                                management of pre-press, printing, finishing, packaging and logistics.</p>
                            <p style="color:black;">Each of our plants is equipped with facilities like special effects,
                                full UV, spot UV, lamination (matt and gloss), foiling, embossing, window pasting,
                                die-cutting – to cater to carton packaging, our core business.</p>
                            <p style="color:black;">Our focus on safety, quality, reliability and performance provides
                                you with a competitive advantage by reducing the time it takes to bring your products to
                                market.</p>
                        </div>
                        <div class="flex flex--hcenter flex--vcenter">
                            <a href="#" class="btn btn--left btn--show open__contact" title="I want one!" rel="nofollow"
                                onclick="playAudio()">Contact </a>
                        </div>
                    </div>
                    <div class="half to__svg" id="anim_character_design">
                        <div class="blobs">
                            <img src="images/1111s.gif" style="width:75%; margin-top: -30%;" class="giF-mg" />
                            <div class="blob light--gray">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="blob light--gray">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="blob light--gray">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Services Section -->

            <!-- Products and metrials section -->
            <!-- <div class="scene section_video_explainer" data-id="Products">
                <div class="scene__inner flex flex--hcenter flex--vcenter">
                    <div class="half"><br /><br />
                        <h1 class="left_border">Products & Material</h1>

                        <div class="text text--small">
                            <p style="color:black;"><strong class="txt_border">FMCG </strong>:-We manufacture trendy printed cartons of all shapes and sizes for your FMCG products that are ideal for point of purchase, retail and wholesale packaging and can withstand contamination
                                and damage. </p>
                            <p style="color:black;"><strong class="txt_border">Pharma </strong>:-Pharma products need to be protected from light, temperature and contamination during transportation, cold storage and at the pharmacy. Our Pharma cartons do all that and are water
                                and moisture resistant too. </p>

                            <p style="color:black;"><strong class="txt_border">Perfume </strong>:-Packaging for perfume bottles need to be attractive and appealing. We manufacture fashionable custom boxes that can increase the visibility of the products in stores and attract
                                more buyers. </p>

                            <p style="color:black;"><strong class="txt_border">Beverages </strong>:-Liquor bottles are fragile and are easily prone to damage. We manufacture sturdy paperboard folding cartons that protect the alcohol bottles from shock and vibration and prevent
                                dirt accumulation and contamination.</p>

                            <p style="color:black;"><strong class="txt_border">Tobacco </strong>:-We manufacture custom cigarettes boxes that are designed keeping in mind the major target audience and add value for your brand.</p>
                            <style>
                                 ::-webkit-scrollbar {
                                    display: none;
                                }
                            </style>

                        </div>
                        <div class="flex flex--hcenter flex--vcenter">
                            <a href="#" class="btn btn--left btn--show open__contact disabled" title="I want one!" rel="nofollow">I want one!</a>
                        </div>
                    </div>
                    <div class="half to__svg" id="anim_video_explainer">
                        <div class="blobs">
                            <div class="blob light--gray">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="blob light--gray">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="blob light--gray">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- Products and metrials section -->

            <!--  Plant list section -->
            <div class="scene section_Plant_list" data-id="Plant_list">
                <div class="scene__inner flex flex--hcenter flex--vcenter">
                    <div class="half" style="width:100%;">
                        <h1 class="left_border plbm">Plant List</h1>
                        <img src="images/11p.gif" style="width: 90%" class="only-mobile" />
                        <div class="text text--small plant_wrape">
                            <div id="sing" class="plnat_wrp">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="plant_slide">
                                                <h3 class="title">PREPRESS</h3>

                                                <h4 style="color:black;" class="comp_txt"><strong>Computer to
                                                        Plate</strong></h4>


                                                <p class="description">To deliver optimum quality, we master the
                                                    following software versions:</p>
                                                <ul class="custom_ul">
                                                    <li>Dainippon Screen CtP (Computer to Plate) for quality printing.
                                                    </li>
                                                    <li>Esko Graphics for packaging print file processing.</li>
                                                    <li>Artois CAD for structural design.</li>
                                                    <li>The colour tone for colour retouching.</li>
                                                    <li>Flex Proof for hard copies.</li>
                                                    <li>Des pack for file finishing.</li>
                                                    <li>Flex Rip for quality control. </li>
                                                </ul>

                                                <h4 style="color:black;" class="comp_txt mt-5"><strong>Sampling
                                                        Table</strong></h4>
                                                <p class="description"> We have the Kongsberg sampling table by Esko,
                                                    which is an automated die-less cutting & creasing table that
                                                    accelerates the sample making through direct inputs from the CAD
                                                    diagram. With the help of this, a first sample can be made, and
                                                    modifications can be done on the die-line before sending it for
                                                    die-making. </p><br>
                                            </div>

                                            <div class="plant_slide">
                                                <h3 class="title">PRESS</h3>

                                                <p class="description">

                                                    Mitsubishi Diamond 1000, 20 x 28 inches, six-colour UV press with
                                                    online coater The printer can print on a range of substrates from
                                                    regular boards to plastic and metallic boards using eco-friendly UV
                                                    curing inks. Online coating of UV and special effects are possible
                                                    on this machine.
                                                </p>
                                                <ul class="custom_ul" style="margin-top: 5px;">
                                                    <li>Mitsubishi Diamond 2000, 22 x 32 inches, five-colour with online
                                                        coater.</li>
                                                    <li>Mitsubishi Diamond 3000, 28 x 40 inches, four-colour press.</li>
                                                    <li>Komori LIthrone, 28 x 28 inches, two-colour press.</li>
                                                </ul>
                                                <br>
                                            </div>

                                            <div class="plant_slide">
                                                <h3 class="title">FINISHING</h3>

                                                <h4 style="color:black;" class="comp_txt"><strong>Lamination - Gloss /
                                                        Matt - Full and Strip</strong></h4>
                                                <p class="description">
                                                    The 28 x 40 inches, fully automatic water base laminator encompasses
                                                    pre-dust cleaning devices, calendaring (to flatten the surface of
                                                    the board) and special auto sheeter for separating laminated sheets
                                                    of high-density films. The machine can laminate films such as BOPP,
                                                    PET, MET PET of thickness ranging from 10 microns to 80 microns. The
                                                    final laminated sheet is ready for the next operation without any
                                                    waiting time.
                                                </p>
                                                <h4 style="color:black;" class="comp_txt"><strong>UV Varnish - Full and
                                                        Spot</strong></h4>
                                                <p class="description">The 28 x 40 inches, automatic coating machine is
                                                    the high-speed auto coater that can perform both spot and full
                                                    varnishes consisting of UV and water-based varnishes on regular
                                                    paper board as well as distinctive surfaces.</p>

                                                <h4 style="color:black;" class="comp_txt"><strong>Hot foil
                                                        stamping</strong></h4>
                                                <p class="description">Heidelberg converted 22 x 32 inches is used for
                                                    hot foil stamping.</p>
                                                <h4 style="color:black;" class="comp_txt"><strong>Die - Cutting</strong>
                                                </h4>
                                                <p class="description">The BOBST SPECIA 106, 29 x 41 inches with
                                                    stripping can process up to 7700 sheets per hour and enables fast
                                                    make-readies using the centre-line feature for accurate positioning.
                                                    The plate system for this technology allows for archiving of files
                                                    for quick reprinting. During redundancy, a job can be changed over,
                                                    started and executed at faster speeds as the original plate is
                                                    preserved for reprinting. This technology allows for faster
                                                    execution of high volume jobs.</p>
                                                <h4 style="color:black;" class="comp_txt"><strong>Folding and
                                                        Gluing</strong></h4>
                                                <p class="description">The BOBST Ambition 76 serves fast, accurate
                                                    folding and bubble-free glueing.</p>
                                                <h4 style="color:black;" class="comp_txt"><strong>Auto Laminator for
                                                        e-flute packaging</strong></h4>
                                                <p class="description">The ultra-sophisticated machine helps in taking
                                                    the printed sheets directly for the next operation without waiting
                                                    for drying time. Compared to the conventional drying system that
                                                    takes up to 12-24 hours, this process is faster as the drying time
                                                    is eliminated. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sbox">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="half to__svg" id="anim_infographic">
                        <div class="blobs"><img src="images/11p.gif" style=" margin-top: -36%;" class="giF-mg" />
                            <div class="blob light--gray">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="blob light--gray">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="blob light--gray">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--  //Plant list section -->


            <!-- client section and clients-->
            <div class="scene section_Clients" data-id="Clients">

                <!--  <div class="scene__inner flex flex--hcenter flex--vcenter"> -->
                <div id="client_wrp" class="container">
                    <h1 class="left_border">Our Clients</h1>
                    <div class="row">

                        <div class="col-md-12">
                            <!-- ------------------------------ -->
                            <div id="owl-demo" class="owl-carousel owl-theme">

                                <div class="item">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/1.png" /></center>
                                        </div>


                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/2.png"
                                                    class="client_3" /></center>
                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/3.png"
                                                    class="client_4" /></center>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/4.png" /></center>
                                        </div>
                                    </div>
                                    <!---SECOND COL CLIENTS-->
                                    <div class="row">

                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/5.png" /></center>
                                        </div>


                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/6.png" /></center>
                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/7.png" /></center>
                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/8.png" /></center>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="row">

                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/9.png" /></center>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/10.png" /></center>
                                        </div>


                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/11a.png" /></center>
                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/12.png" /></center>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <center><img class="client_logo" src="images/clogos/13.png" /></center>
                                        </div>


                                        <!-- <div class="col-md-3 col-sm-4 col-xs-6">
												<center><img class="client_logo" src="images/client_15.JPG" /></center>
											</div>

											<div class="col-md-3 col-sm-4 col-xs-6">
												<center><img class="client_logo" src="images/client_16.JPG" /></center>
											</div> -->

                                    </div>
                                </div>


                            </div>

                            <!-- ------------------------------ -->
                        </div>

                    </div>
                </div>


                <!--
					<div class="half">
					<h1 class="left_border">Our Clients</h1>
					<div class="text text--small"><img src="images/client1.jpg" style="margin-top:-6%;width:100%;"/></div>


					</div>
					<div class="half to__svg" id="anim_micro_animation">-->
                <div class="blobs">
                    <div class="blob light--gray">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="blob light--gray">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="blob light--gray">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <!-- </div>
					</div> -->
                <!-- </div> -->
            </div>
            <!-- //client section and clients-->

            <!-- testmonials section   -->
            <div class="scene section_Testimonials" data-id="Testimonials">
                <div class="scene__inner flex flex--hcenter flex--vcenter">
                    <div class="half" style="height:none !important;"><br /><br />
                        <h1 class="left_border">Testimonials</h1>
                        <img src="images/testimonial0.png" class="only-mobile"
                            style="width:75%; margin-bottom: 15px; margin-top: -20px">
                        <div class="text text--small">

                            <div id="sing" class="test_wrp">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div id="testimonial-slider" class="owl-carousel">
                                                <div class="testimonial">
                                                    <div class="pic">
                                                        <img src="images/img-1.jpg" alt="">
                                                    </div>
                                                    <p class="description">
                                                        Singhania Printers has been our primary supplier of custom
                                                        display products for several years. Their creative designers go
                                                        above and beyond to quickly turn our ideas into physical
                                                        samples, which help us to secure plenty of promotional sales. I
                                                        can sincerely say that Singhania’s service is the best I have
                                                        experienced in the 8 years I have been a buyer of corrugated
                                                        products!.
                                                    </p>
                                                    <div class="testimonial-prof">
                                                        <span class="title">williamson</span>
                                                        <small>Web Developer</small>
                                                    </div>
                                                </div>

                                                <div class="testimonial">
                                                    <div class="pic">
                                                        <img src="images/img-1.jpg" alt="">
                                                    </div>
                                                    <p class="description">
                                                        Singhania Printers worked with us for many months until we had
                                                        the perfect packaging for our product. They went above and
                                                        beyond every step of the way, and we are very pleased with the
                                                        final results. I’d highly recommend them, especially for those
                                                        looking for quality packaging that is ‘Made in India’.
                                                    </p>
                                                    <div class="testimonial-prof">
                                                        <span class="title">williamson</span>
                                                        <small>Web Developer</small>
                                                    </div>
                                                </div>

                                                <div class="testimonial">
                                                    <div class="pic">
                                                        <img src="images/img-1.jpg" alt="">
                                                    </div>
                                                    <p class="description">
                                                        Working with Singhania was a dream. As a new business, we rely
                                                        on vendors not just for their service, but for the expertise and
                                                        education they offer. The team were super knowledgeable,
                                                        flexible and patient. I also appreciated their readiness to work
                                                        with low volumes and that they have the capacity to handle so
                                                        much more as we grow.
                                                    </p>
                                                    <div class="testimonial-prof">
                                                        <span class="title">kristiana</span>
                                                        <small>Web Designer</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="half to__svg" id="anim_all_in">
                        <div class="blobs">
                            <img src="images/testimonial0.png" style="width:80%;margin-top:-36%;" class="giF-mg">
                            <div class="blob light--gray">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="blob light--gray">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="blob light--gray">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //testmonials section   -->


            <!-- Portifilo Section -->
            <div class="scene section_portfolio" data-id="Portfolio">

                <div class="scene__inner flex flex--hcenter flex--vcenter">
                    <div class="half"><br /><br />
                        <h1 class="left_border">Products & Material</h1>
                        <div class="text text--small">
                            <p style="color:black;">Consumers have many options. Make your product the one they choose.
                                Whether it be custom Product packaging for Retail, On-Line Sales, Business to Business,
                                Marketing packaging, and Promotional or Presentation packaging, at Singhania, we have a
                                wealth of experience to assist you in making the best choices to highlight your
                                products.</p>
                            <p style="color:black;">It is a fact that a reader will be distracted by the readable
                                content of a page when looking at its layout. Whether your product label has established
                                requirements, or if you are looking for assistance developing a fresh approach,
                                Singhania Offset Printers is the ideal partner for you.</p>
                        </div>
                        <div class="flex flex--hcenter flex--vcenter">
                            <a href="#" class="btn btn--left btn--show open__contact" title="I want one!" rel="nofollow"
                                onclick="playAudio()">Contact </a>
                        </div>
                    </div>
                    <div class="half to__svg" id="mustdis">
                        <div class="slider_wrp">

                            <div class="Container">
                                <!-- Carousel Container -->
                                <div class="SlickCarousel">
                                    <!-- <div class="Arrows"></div> -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/1.jpg">
                                            </div>
                                            <h3><a href="#pitem-1" rel="modal:open">Redihealth</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/2.jpg">
                                            </div>
                                            <h3><a href="#pitem-2" rel="modal:open">Ibiza</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/3.jpg">
                                            </div>
                                            <h3><a href="#pitem-3" rel="modal:open">Biscrok Biscuits</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/4.jpg">
                                            </div>
                                            <h3><a href="#pitem-4" rel="modal:open">Choco Blitz</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/5.jpg">
                                            </div>
                                            <h3><a href="#pitem-5" rel="modal:open">Sunfeast Dark Fantasy</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/6.jpg">
                                            </div>
                                            <h3><a href="#pitem-6" rel="modal:open">Raaga Facial Kit</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/7.jpg">
                                            </div>
                                            <h3><a href="#pitem-7" rel="modal:open">Ankit Deluxe chocolates</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/8.jpg">
                                            </div>
                                            <h3><a href="#pitem-8" rel="modal:open">Dixcy Scott</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/9.jpg">
                                            </div>
                                            <h3><a href="#pitem-9" rel="modal:open">Cintu Royal Milk</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/10.jpg">
                                            </div>
                                            <h3><a href="#pitem-10" rel="modal:open">Hydroheal AM Gel</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/11.jpg">
                                            </div>
                                            <h3><a href="#pitem-11" rel="modal:open">Prithvi’s innerwear</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/12.jpg">
                                            </div>
                                            <h3><a href="#pitem-12" rel="modal:open">Fairever fairness cream</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/13.jpg">
                                            </div>
                                            <h3><a href="#pitem-13" rel="modal:open">OME3</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/14.jpg">
                                            </div>
                                            <h3><a href="#pitem-14" rel="modal:open">Pepe</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/15.jpg">
                                            </div>
                                            <h3><a href="#pitem-15" rel="modal:open">Candyman</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                    <!-- Item -->
                                    <div class="ProductBlock">
                                        <div class="Content">
                                            <div class="img-fill">
                                                <img src="images/products/16.jpg">
                                            </div>
                                            <h3><a href="#pitem-16" rel="modal:open">Cintu Vanilla Cupcakes</a></h3>
                                        </div>
                                    </div>
                                    <!-- Item -->
                                </div>
                                <!-- Carousel Container -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bottom-left" style="display: none;">
                    <a class="back_to_home" href="#" id="start2" onclick="playAudio()">
                        <span>To the map</span>
                    </a>
                </div>
            </div>
            <!-- End of Portfolio Section -->

            <!-- Blogs Section -->
            <div class="scene section_Blogs" data-id="Blogs">

                <div class="scene__inner flex flex--hcenter flex--vcenter">
                    <div class="blog_wrapper">
                        <div class="container">
                            <?php require_once("blogs.php"); ?>
                        </div>
                    </div>

                </div>
            </div>
            <!-- //Blogs Section -->

            <!-- Contact us Section -->
            <div class="scene section_contactus" data-id="contactus">
                <div class="scene__inner flex flex--hcenter flex--vcenter" style="display: none !important;"></div>
                <div>
                    <div class="blog_wrapper contact_wrap">
                        <h1 class="left_border">Contact Us</h1>
                        <div class="container">
                            <!-- ------ -->
                            <div class="row">
                                <div class="col-md-6">
                                    <form class="form form-animate-fields" action="#">
                                        <div class="form-field">
                                            <input class="form-input" type="text" name="firstname" id="firstname">
                                            <label for="firstname" class="form-label">
                                                <span class="form-label-content">Your Full Name</span>
                                            </label>
                                        </div>
                                        <div class="form-field">
                                            <input class="form-input" type="email" name="email" id="email">
                                            <label for="email" class="form-label">
                                                <span class="form-label-content">E-mail Address</span>
                                            </label>
                                        </div>
                                        <div class="form-field">
                                            <input class="form-input" type="text" name="reqi" id="reqi">
                                            <label for="reqi" class="form-label">
                                                <span class="form-label-content">Your Requirement</span>
                                            </label>
                                        </div>
                                        <div class="form-field">
                                            <input class="form-input" type="text" name="lastname" id="lastname">
                                            <label for="lastname" class="form-label">
                                                <span class="form-label-content">Your Message</span>
                                            </label>
                                        </div>

                                        <div class="field field--submit">
                                            <div class="abs">
                                                <div class="loader">
                                                    <div class="flex flex--vcenter flex--hcenter">
                                                        <span></span>
                                                        <span></span>
                                                        <span></span>
                                                    </div>
                                                </div>
                                                <div class="response">
                                                    <div class="response__error">Sorry, we couldn't send your message.
                                                    </div>
                                                    <div class="response__ok">Thanks! We'll get back to you as soon as
                                                        possible.</div>
                                                </div>
                                            </div>
                                            <span class="btn"><input type="submit" onclick="playAudio()" value="Submit"
                                                    class="txt input--submit" style="color:white;"></span>


                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-6">
                                    <div class="con-hf2">

                                        <ul class="contact-list">
                                            <li class="list-item"><i class="fa fa-map-marker fa-2x ffsz"><span
                                                        class="contact-text place"> Sno 340, Sri Krupa Market, Mahbub
                                                        Mansion, Malakpet, Malakpet, Hyderabad, Telangana
                                                        500036</span></i></li>

                                            <li class="list-item"><i class="fa fa-phone fa-2x ffsz"><span
                                                        class="contact-text phone"><a href="tel:1-212-555-5555"
                                                            title="Give me a call">040 2454 4037</a></span></i></li>

                                            <li class="list-item"><i class="fa fa-envelope fa-2x ffsz"><span
                                                        class="contact-text gmail"><a href="mailto:#"
                                                            title="Send me an email">info@singhaniaoffsetprinters.com</a></span></i>
                                            </li>

                                        </ul>

                                        <hr>
                                        <ul class="social-media-list">
                                            <li>
                                                <a href="https://www.facebook.com/singhaniaprinters/" target="_blank"
                                                    class="contact-icon">
                                                    <i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" target="_blank" class="contact-icon">
                                                    <i class="fa fa-twitter" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" target="_blank" class="contact-icon">
                                                    <i class="fa fa-instagram" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="https://www.linkedin.com/company/singhania-offset-printers-pvt-ltd"
                                                    target="_blank" class="contact-icon">
                                                    <i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                            </li>
                                        </ul>
                                        <hr>


                                    </div>
                                </div>

                            </div>

                            <!-- ------ -->
                        </div>
                    </div>

                </div>
            </div>
            <!-- //Contact us Section -->

            <!-- About Section -->
            <div class="scene section_about" data-id="about">

                <div class="scene__inner flex flex--hcenter flex--vcenter">

                    <div class="half" style="width:100%;"><br><br>
                        <h1 class="left_border">Who We Are</h1>
                        <img src="images/about-us1.gif" class="only-mobile"
                            style="width: 90%; margin-top: -10px; margin-bottom: 10px">
                        <div class="text text--small">
                            <p style="color:black;">Since 1982, we've been carrying a legacy of custom printing and
                                packaging services in India. Our environmentally sustainable organization company has
                                grown many folds since then and in 2009, moved to a spacious 100,000 sq.ft plant
                                situated at Apparel Export Park near Kompally, Hyderabad. </p>

                            <p style="color:black;">Founded by the able hands of Mr Mukund Lal Singhania, Singhania
                                Offset Printers stands as a pioneer in the packaging industry. It is now managed by the
                                second generation entrepreneurs Nitin and Rajan Singhania, sons of the founder, who
                                strive to make the highest quality, safest packaging in the most responsible way – for
                                brands; we're passionate about.</p>

                            <p style="color:black;">From our high-end promotional custom packaging to our
                                economically-priced standardized packaging, every package provides you with form,
                                function, and a terrific branding opportunity! Today, Singhania Printers is labelled as
                                the one-stop-shop for custom printing and packaging solutions in India.
                            </p>
                        </div>

                        <h3 class="title">Quality</h3>
                        <div class="text text--small">
                            <p style="color:black;">Our take on Quality as a differentiator exemplifies in our
                                sophisticated laboratory with high-precision testing instruments that ensure consistent,
                                high-quality outputs and full compliance with the customers' quality demands.</p>
                        </div>

                        <h3 class="title">Quick Turnaround</h3>
                        <div class="text text--small">
                            <p style="color:black;">Powered by sophisticated printing technology, we deliver a ready
                                sheet for the next operation with zero waiting time, thus ensuring ultra-fast outputs.
                            </p>
                        </div>

                        <h3 class="title">One-Stop Shop</h3>
                        <div class="text text--small">
                            <p style="color:black;">We offer high-quality solutions for your complete end-to-end
                                'design-print-pack' needs. Provide us with the print file, and we take care of the rest.
                                We accept, process, carry out and deliver your order as per your instructions and
                                requirements.</p>
                        </div>
                    </div>
                    <div class="sbox">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="half to__svg " id="anim_character_design">
                        <div class="blobs big_rock abbt">

                            <img src="images/about-us1.gif">

                        </div>
                    </div>
                </div>
            </div>
            <!-- //About Section -->



            <div class="fixed name" id="current"></div>
        </div>
    </div>



    <!-- footer section for single page aboutus page -->
    <footer id="footer" data-function="about">
        <div class="shape">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="ctc"
                viewBox="0 0 184.8 158.2" xml:space="preserve">
                <g id="ctc_1">
                    <path fill="#3116F7" id="ctc_path"
                        d="M184.8,21.7v136.5H53c-14.2-6-33.1-22.9-35.5-38.4C12.9,90,31.9,68.6,60.2,70.6c8.2,0.6,15.4,4.6,18.6,6   c37.6,17.5,39.3,0.4,55-18.4C151.3,37.2,165.2,30,184.8,21.7z" />
                </g>
                <!-- <g class="hidden" id="ctc_2">
				<path fill="#2A11DE" d="M184.8,21.7v136.5H53c-14.2-6-33.1-22.9-35.5-38.4C12.9,90,31.9,68.6,60.2,70.6c8.2,0.6,15.4,4.6,18.6,6   c37.6,17.5,39.3,0.4,55-18.4C151.3,37.2,165.2,30,184.8,21.7z"/>
				</g> -->
            </svg>
        </div>
       <!--  <span class="pos--fixed flex flex--hcenter flex--vcenter" id="ctc_text">
            <svg width="37" height="22">
                <use xlink:href="#icon-email"></use>
            </svg>
            <span>Get something<br> similar!</span>
            </span> -->


        <span class="pos--fixed flex flex--hcenter flex--vcenter page_text about" onclick="playAudio()">
            <span onclick="playAudio()">About Us</span>
        </span>

        <span class="pos--fixed flex flex--hcenter flex--vcenter page_text portfolio" onclick="playAudio()">
            <span onclick="playAudio()" class="foot_txt">Product &amp; Material</span>
        </span>

        <span class="pos--fixed flex flex--hcenter flex--vcenter page_text Services" onclick="playAudio()">
            <span onclick="playAudio()" class="foot_txt">Services</span>
        </span>

        <span class="pos--fixed flex flex--hcenter flex--vcenter page_text Plant_list" onclick="playAudio()">
            <span onclick="playAudio()" class="foot_txt">Plant List</span>
        </span>
    </footer>

    <div class="follow blob blob--small" id="follow">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
    <!-- //footer section for single page aboutus page -->


    <!-- auto pull down menu section -->
    <div class="popup flex flex--hcenter" id="popup">
        <div class="popup__parts" id="popup__parts"></div>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" id="contact">
            <path class="splash_4" id="splash_4" d=""></path>
            <path class="splash_5" id="splash_5" d=""></path>
        </svg>
        <div class="flex" id="popup__inner">
            <div class="left half flex flex--hcenter flex--vcenter flex--column">
                <div class="title text--small" style="color: #222">
                    Contact us
                </div>
                <div class="popup__container">
                    <form class="text--big">
                        <div class="field field--text">
                            <div class="placeholder text--big" style="color: #222">Your work email</div>
                            <input type="email" name="mail" class="input--text" autocomplete="off"
                                oninvalid="this.setCustomValidity('This field is required')" required>
                            <label for="mail">
                                <span class="error--text" style="color: #222">Email is invalid.</span>
                                <span class="label__content label--small" style="color: #222">Your work email</span>
                            </label>
                        </div>
                        <div class="field field--text input--high" style="color: #222">
                            We would love to hear from you
                        </div>

                        <div class="filed field--privacy" style="color: #222">
                            Whether you have a question about the services we provide, the products we offer or the
                            materials we use for our products, our team is ready to answer them all.
                        </div>

                        <div class="field field--submit">
                            <span class="btn"><input type="submit" value="Submit" class="txt input--submit"
                                    style="color:white;"></span>
                            </span>
                            <div class="abs">
                                <div class="loader" style="top: 64px">
                                    <div class="flex flex--vcenter flex--hcenter">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="response" style="position: absolute; top: -58px;">
                                    <div class="response__error">Sorry, we couldn't send your message.</div>
                                    <div class="response__ok">Thanks! We'll get back to you as soon as possible.</div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <div class="right half flex flex--hcenter flex--vcenter flex--column">
                <div class="title text--small" onclick="playAudio()">
                    Menu
                </div>
                <ul class="text--big" id="nav">
                    <li><a href="index.html" rel="nofollow" title="About Us" data-id="about" onclick="playAudio()">About
                            Us</a></li>
                    <li><a href="index.html" rel="nofollow" title="Services" data-id="Services"
                            onclick="playAudio()">Services</a></li>
                    <li><a href="index.html" rel="nofollow" title="Infographic" data-id="Plant_list"
                            onclick="playAudio()">Plant & Machinery</a></li>
                    <li><a href="index.html" rel="nofollow" title="Product & Material" data-id="portfolio"
                            onclick="playAudio()">Product & Material</a></li>
                    <!-- <li><a href="index.html" rel="nofollow" title="Video explainer" data-id="video_explainer" onclick="playAudio()">Product & Material</a></li>-->

                    <li><a href="index.html" rel="nofollow" title="Blogs" data-id="Blogs"
                            onclick="playAudio()">Blogs</a></li>

                    <li><a href="index.html" rel="nofollow" title="Clients" data-id="Clients"
                            onclick="playAudio()">Clients</a></li>
                    <li><a href="index.html" rel="nofollow" title="Testimonials" data-id="Testimonials"
                            onclick="playAudio()">Testimonials</a></li>
                    <!-- <li><a href="index.html" rel="nofollow" title="Contact Us" data-id="portfolio" onclick="playAudio()" >Contact Us</a></li>-->
                    <li> <a href="index.html" class="" title="I want one!" rel="nofollow" data-id="contactus"
                            onclick="playAudio()">Contact Us</a></li>

                </ul>
                <div class="socials">
                    <a href="https://www.facebook.com/singhaniaprinters/" target="_blank" title="Facebook"
                        onclick="playAudio()">
                        <svg width="30" height="30">
                            <use xlink:href="#icon-facebook"></use>
                        </svg>
                    </a>
                    <a href="#" target="_blank" title="Instagram" onclick="playAudio()">
                        <svg width="30" height="30">
                            <use xlink:href="#icon-instagram"></use>
                        </svg>
                    </a>
                    <a href="https://www.linkedin.com/company/singhania-offset-printers-pvt-ltd" target="_blank"
                        title="linkedin" onclick="playAudio()">
                        <img src="all_svgs/linkedin-logo.svg">
                    </a>
                    <a href="#" target="_blank" title="twitter" onclick="playAudio()">
                        <img src="all_svgs/twitter.svg">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- //auto pull down menu section -->


    <!-- Contact pop page section -->
    <div class="contact">
        <div class="contact__flex flex flex--hcenter flex--vcenter flex--column">
            <div class="bg__ie"></div>
            <div class="title text--small" onclick="playAudio()">
                Contact
            </div>
            <form class="text--big">
                <div class="field field--text">
                    <div class="placeholder text--big">Your work email</div>
                    <input type="email" name="mail" class="input--text" autocomplete="off"
                        oninvalid="this.setCustomValidity('This field is required')" required>
                    <label for="mail">
                        <span class="error--text">Email is invalid.</span>
                        <span class="label__content label--small">Your work email</span>
                    </label>
                </div>
                <div class="field field--text input--high">
                    We would love to hear from you
                </div>

                <div class="filed field--privacy">
                    Whether you have a question about the services we provide, the products we offer or the materials we
                    use for our products, our team is ready to answer them all.
                </div>

                <div class="field field--submit">
                    <span class="btn"><input type="submit" onclick="playAudio()" value="Submit"
                            class="txt input--submit" style="color:white;"></span>
                    </span>
                    <div class="abs">
                        <div class="loader">
                            <div class="flex flex--vcenter flex--hcenter">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                        <div class="response">
                            <div class="response__error">Sorry, we couldn't send your message.</div>
                            <div class="response__ok">Thanks! We'll get back to you as soon as possible.</div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="socials">
                <a href="https://www.facebook.com/singhaniaprinters/" target="_blank" title="Facebook"
                    onclick="playAudio()">
                    <svg width="30" height="30">
                        <use xlink:href="#icon-facebook"></use>
                    </svg>
                </a>
                <a href="#" target="_blank" title="Instagram" onclick="playAudio()">
                    <svg width="30" height="30">
                        <use xlink:href="#icon-instagram"></use>
                    </svg>
                </a>
                <a href="https://www.linkedin.com/company/singhania-offset-printers-pvt-ltd" target="_blank"
                    title="linkdein" onclick="playAudio()">
                    <img src="all_svgs/linkedin-logo.svg">
                </a>
                <a height="30" width="30" href="#" target="_blank" title="twitter" onclick="playAudio()">
                    <img src="all_svgs/twitter.svg">
                </a>
            </div>
        </div>
    </div>

    <svg xmlns="http://www.w3.org/2000/svg" width="0" height="0" version="1.1">
        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9"
                    result="goo" />
                <feComposite in="SourceGraphic" in2="goo" operator="atop" />
            </filter>
        </defs>
        <defs>
            <filter id="goo2">
                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9"
                    result="goo" />
                <feComposite in="SourceGraphic" in2="goo2" operator="atop" />
            </filter>
        </defs>
        <symbol id="icon-facebook" viewBox="0 0 90 90">
            <path
                d="M90 15.001C90 7.119 82.884 0 75 0H15C7.116 0 0 7.119 0 15.001v59.998C0 82.881 7.116 90 15.001 90H45V56H34V41h11v-5.844C45 25.077 52.568 16 61.875 16H74v15H61.875C60.548 31 59 32.611 59 35.024V41h15v15H59v34h16c7.884 0 15-7.119 15-15.001V15.001z" />
        </symbol>
        <symbol id="icon-instagram" viewBox="0 0 169.063 169.063">
            <g>
                <path
                    d="M122.406 0H46.654C20.929 0 0 20.93 0 46.655v75.752c0 25.726 20.929 46.655 46.654 46.655h75.752c25.727 0 46.656-20.93 46.656-46.655V46.655C169.063 20.93 148.133 0 122.406 0zm31.657 122.407c0 17.455-14.201 31.655-31.656 31.655H46.654C29.2 154.063 15 139.862 15 122.407V46.655C15 29.201 29.2 15 46.654 15h75.752c17.455 0 31.656 14.201 31.656 31.655v75.752z" />
                <path
                    d="M84.531 40.97c-24.021 0-43.563 19.542-43.563 43.563 0 24.02 19.542 43.561 43.563 43.561s43.563-19.541 43.563-43.561c0-24.021-19.542-43.563-43.563-43.563zm0 72.123c-15.749 0-28.563-12.812-28.563-28.561 0-15.75 12.813-28.563 28.563-28.563s28.563 12.813 28.563 28.563c0 15.749-12.814 28.561-28.563 28.561zm45.39-84.842c-2.89 0-5.729 1.17-7.77 3.22a11.053 11.053 0 0 0-3.23 7.78c0 2.891 1.18 5.73 3.23 7.78 2.04 2.04 4.88 3.22 7.77 3.22 2.9 0 5.73-1.18 7.78-3.22 2.05-2.05 3.22-4.89 3.22-7.78 0-2.9-1.17-5.74-3.22-7.78-2.04-2.05-4.88-3.22-7.78-3.22z" />
            </g>
        </symbol>
        <symbol id="icon-behance" viewBox="0 0 49.652 49.652">
            <g>
                <path
                    d="M20.894 20.686c0-1.9-1.293-1.9-1.293-1.9H14.28v4.078h4.991c.863 0 1.623-.275 1.623-2.178zm12.327 2.178c-2.821 0-3.215 2.814-3.215 2.814h6.002c0 .002.037-2.814-2.787-2.814zm-13.62 2.816h-5.318v4.886h4.711c.08 0 .198.004.339 0 .757-.019 2.196-.235 2.196-2.374 0-2.536-1.928-2.512-1.928-2.512z" />
                <path
                    d="M24.826 0C11.137 0 0 11.137 0 24.826c0 13.688 11.137 24.826 24.826 24.826 13.688 0 24.826-11.138 24.826-24.826C49.652 11.137 38.516 0 24.826 0zm4.393 16.615h7.539v2.251h-7.539v-2.251zm-3.812 11.804c0 5.577-5.806 5.395-5.806 5.395h-9.508V15.539h9.508c2.891 0 5.172 1.597 5.172 4.867 0 3.269-2.788 3.477-2.788 3.477 3.675-.001 3.422 4.536 3.422 4.536zm14.105-.3h-9.471c0 3.396 3.217 3.182 3.217 3.182 3.036 0 2.93-1.966 2.93-1.966h3.219c0 5.218-6.254 4.859-6.254 4.859-7.503 0-7.021-6.985-7.021-6.985s-.007-7.022 7.021-7.022c7.396 0 6.357 7.932 6.359 7.932z" />
            </g>
        </symbol>
        <symbol id="icon-dribbble" viewBox="0 0 438.533 438.533">
            <path
                d="M409.133 109.203c-19.608-33.592-46.205-60.189-79.798-79.796C295.736 9.801 259.058 0 219.273 0c-39.781 0-76.47 9.801-110.063 29.407-33.595 19.604-60.192 46.201-79.8 79.796C9.801 142.8 0 179.489 0 219.267c0 39.78 9.804 76.463 29.407 110.062 19.607 33.592 46.204 60.189 79.799 79.798 33.597 19.605 70.283 29.407 110.063 29.407s76.47-9.802 110.065-29.407c33.593-19.602 60.189-46.206 79.795-79.798 19.603-33.596 29.403-70.284 29.403-110.062.001-39.782-9.8-76.472-29.399-110.064zM219.27 31.977c47.201 0 88.41 15.607 123.621 46.82l-3.569 4.993c-1.427 2.002-4.996 5.852-10.704 11.565-5.709 5.708-11.943 11.136-18.699 16.274-6.762 5.14-15.94 10.992-27.555 17.559-11.611 6.567-23.982 12.328-37.117 17.276-21.887-40.355-45.296-76.709-70.231-109.064 15.039-3.616 29.789-5.423 44.254-5.423zM72.524 103.06c18.271-23.026 40.537-40.73 66.806-53.1 23.601 31.405 46.82 67.381 69.662 107.921-57.862 15.227-115.532 22.841-173.014 22.838 6.094-28.739 18.275-54.628 36.546-77.659zM44.54 286.794c-8.376-21.412-12.563-43.923-12.563-67.527 0-2.666.098-4.665.286-5.996 68.905 0 132.955-8.848 192.149-26.553 6.092 11.8 11.136 22.364 15.133 31.693-.771.38-1.999.806-3.713 1.283-1.719.476-2.953.806-3.721.999l-10.561 3.711c-7.236 2.666-16.708 7.235-28.409 13.703-11.704 6.478-24.123 14.182-37.257 23.13-13.134 8.949-26.696 20.797-40.684 35.553-13.99 14.75-25.743 30.591-35.26 47.53-15.227-16.939-27.026-36.12-35.4-57.526zM219.27 406.56c-44.54 0-84.32-14.277-119.343-42.825l4.283 3.142c6.661-14.66 16.462-28.746 29.408-42.257 12.944-13.511 25.41-24.413 37.401-32.695 11.991-8.274 25.028-16.077 39.115-23.414 14.084-7.323 23.691-11.991 28.835-13.983 5.14-1.998 9.233-3.572 12.278-4.716l.568-.287h.575c18.647 48.916 31.977 96.313 39.968 142.184-23.602 9.902-47.961 14.848-73.088 14.851zm157.606-86.081c-14.086 21.796-31.696 39.834-52.817 54.104-7.81-43.776-19.985-88.415-36.549-133.902 37.877-5.907 76.8-3.142 116.771 8.274-4.189 25.886-13.326 49.732-27.405 71.524zm26.83-103.781a232.46 232.46 0 0 0-7.139-1.283c-2.854-.473-6.331-1.047-10.424-1.713-4.087-.666-8.662-1.283-13.702-1.855a549.162 549.162 0 0 0-16.136-1.569c-5.708-.478-11.8-.855-18.268-1.143a449.763 449.763 0 0 0-19.705-.428c-6.656 0-13.657.193-20.981.571a246.549 246.549 0 0 0-21.265 1.999c-.575-.953-1.287-2.524-2.143-4.714-.855-2.187-1.479-3.855-1.848-4.997a1895.128 1895.128 0 0 0-12.573-27.121c13.134-5.333 25.652-11.469 37.555-18.418 11.892-6.949 21.402-13.131 28.544-18.555a237.319 237.319 0 0 0 20.27-17.277c6.379-6.09 10.513-10.323 12.423-12.703a286.978 286.978 0 0 0 5.424-6.995l.287-.288c27.788 33.88 41.974 72.897 42.538 117.059l-2.857-.57z" />
        </symbol>
        <symbol id="icon-email" viewBox="0 0 485.411 485.411">
            <path
                d="M0 81.824v321.763h485.411V81.824H0zm242.708 198.702L43.612 105.691h398.187L242.708 280.526zm-79.311-37.877L23.867 365.178V120.119l139.53 122.53zm18.085 15.884l61.22 53.762 61.22-53.762L441.924 379.72H43.487l137.995-121.187zm140.526-15.878l139.535-122.536v245.059L322.008 242.655z" />
        </symbol>
    </svg>
    <!-- //contact page section -->



    <!-- Models for product items -->
    <div id="pitem-1" class="modal">
        <p>Redihealth by Dr. Reddy’s Laboratory is a drug used for the treatment of anemias of nutritional origin</p>
    </div>
    <div id="pitem-2" class="modal">
        <p>Ibiza Extra Suave is an active carbon cigarette brand</p>
    </div>
    <div id="pitem-3" class="modal">
        <p>Biscrok Biscuits are delightfully crunchy and nutritious dog biscuits, perfect for daily training sessions.
        </p>
    </div>
    <div id="pitem-4" class="modal">
        <p>Choco Blitz is a refreshing chocolate toffee that's fun to have any time of the day.
        </p>
    </div>
    <!-- item start -->
    <div id="pitem-5" class="modal">
        <p>Sunfeast Dark Fantasy is a signature cookie recipe by ITC master chefs.</p>
    </div>
    <!-- item End -->
    <!-- item start -->
    <div id="pitem-6" class="modal">
        <p>Raaga Facial Kit is a hygienic facial glow pack to get flawless skin. </p>
    </div>
    <!-- item End -->
    <!-- item start -->
    <div id="pitem-7" class="modal">
        <p> Ankit Deluxe chocolates are homemade chocolates and flavored candies. </p>
    </div>
    <!-- item End -->
    <!-- item start -->
    <div id="pitem-8" class="modal">
        <p>Dixcy Scott is a premium men’s innerwear brand in India.</p>
    </div>
    <!-- item End -->
    <!-- item start -->
    <div id="pitem-9" class="modal">
        <p>Cintu Royal Milk is a rich-milk choco bar for kids.</p>
    </div>
    <!-- item End -->
    <!-- item start -->
    <div id="pitem-10" class="modal">

        <p>Hydroheal AM Gel is a skin antiseptic and disinfectant.</p>
    </div>
    <!-- item End -->
    <!-- item start -->
    <div id="pitem-11" class="modal">
        <p>Prithvi’s innerwear is a premium women’s innerwear brand in India. </p>
    </div>
    <!-- item End -->
    <!-- item start -->
    <div id="pitem-12" class="modal">
        <p>Fairever fairness cream is a product of Cavincare.</p>
    </div>
    <!-- item End -->
    <!-- item start -->
    <div id="pitem-13" class="modal">
        <p>OME3 is a dietary supplement that reduces the risk of coronary heart disease. </p>
    </div>
    <!-- item End -->
    <!-- item start -->
    <div id="pitem-14" class="modal">
        <p>Pepe, It’s my life is a fruit and nut infused choco bar.</p>
    </div>
    <!-- item End -->
    <!-- item start -->
    <div id="pitem-15" class="modal">
        <p>Candyman is a celebrated brand name for Confectionery from the house of ITC Foods. </p>
    </div>
    <!-- item End -->
    <!-- item start -->
    <div id="pitem-16" class="modal">
        <p>Cintu Vanilla Cupcakes is the premium confectionery brand in India.</p>
    </div>
    <!-- item End -->
    <!-- End of models for product items -->


    <!-- All scripts -->
    <script src="wp-content/themes/animat/dist/js/jquery.min.js" type="text/javascript"></script>
    <script src="wp-content/themes/animat/dist/js/header_textca8b.js" type="text/javascript"></script>
    <script src="wp-content/themes/animat/dist/js/bxslider.js" type="text/javascript"></script>
    <!-- <script src="wp-content/themes/animat/dist/js/slick.js" type="text/javascript"></script> -->

    <script type="text/javascript" src="wp-content/themes/animat/dist/js/slick.min.js"></script>
    <script type="text/javascript" src="wp-content/themes/animat/dist/js/owl.carousel.min.js"></script>


    <script src="wp-content/themes/animat/dist/js/bodymovin.js" type="text/javascript" defer></script>
    <script src="wp-content/themes/animat/dist/js/splashf9e3.js" type="text/javascript" defer></script>
    <script src="wp-content/themes/animat/dist/js/mapca8b.js" type="text/javascript" defer></script>
    <script src="wp-content/themes/animat/src/js/scripts/eventsf9e3.js" type="text/javascript" defer></script>
    <script src="wp-content/themes/animat/dist/js/browserf9e3.js" type="text/javascript" defer></script>
    <script src="wp-content/themes/animat/dist/js/jquery.modal.min.js"></script>
    <script src="wp-content/themes/animat/dist/js/svgf9e3.js" type="text/javascript" defer></script>

    <script src="scripts.js" type="text/javascript" defer></script>

    <script>
    var animated = $(".introAnim");

    function beginAnim() {
        $("#animLogo").fadeOut();
        introAnimation();
    }

    function introAnimation() {
        $(".introAnim:eq(0)").animate({
            width: 0
        }, 800);
        $(".introAnim:eq(1)").animate({
            width: 0
        }, 1400);
        $(".introAnim:eq(2)").animate({
            width: 0
        }, 1000);
        $(".introAnim:eq(3)").animate({
            width: 0
        }, 1200);
        setTimeout(endAnim, 1500);
    }

    function endAnim() {
        $(".screenloader").remove();
    }

    setTimeout(beginAnim, 1500);
    </script>

</body>


</html>