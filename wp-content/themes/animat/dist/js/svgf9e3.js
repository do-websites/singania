"use strict";
! function() {
    function t() { ctc_path.setAttribute("d", o) }

    function c() { ctc_path.setAttribute("d", u) }

    function e() {} var n = document.getElementById("ctc"),
        o = ($("footer.functForm"), $(".contact"), document.getElementById("ctc_path"), "M184.8,21.7v136.5H53c-14.2-6-33.1-22.9-35.5-38.4C12.9,90,31.9,68.6,60.2,70.6c8.2,0.6,15.4,4.6,18.6,6   c37.6,17.5,39.3,0.4,55-18.4C151.3,37.2,165.2,30,184.8,21.7z"),
        u = "M184.8,0v158.2H32.1c-16.4-6.9-28.7-21.9-31.5-39.9c-5.4-34.4,22-63.1,54.8-61.5c4.2,0.2,8.2,0.9,12.1,2c21.1,6.1,43.4-0.1,58.1-16.4C141.9,24.2,162.1,9.7,184.8,0z";
    n.addEventListener("mouseout", t), n.addEventListener("mouseover", c), $(".open__contact").on("click", e) }();