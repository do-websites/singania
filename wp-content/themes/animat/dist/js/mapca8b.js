"use strict";
! function() {
    function e(e) {
        var o = e + "_hover",
            s = document.getElementById(o),
            a = C.indexOf(e);
        s && ($("#" + o).show(), s.setAttribute("class", "layer--hidden activeRoom")), b = 1, t(), i(e), n(e), l(e), $("#footer").fadeIn(), d(a), f()
    }

    function t() { setTimeout(function() { $("#jsRooms").removeClass("active"), $("#jsRooms").fadeOut(), m.className = "active", $(".footer #ctc_text").delay(300).fadeIn() }, 300) }

    function o() {
        if (!b) {
            var e = document.getElementsByClassName("activeRoom");
            e.length && ($("body").hasClass("ie") || $("body").hasClass("firefox") ? $(".activeRoom").hide() : $(".activeRoom").fadeOut(200), e[0].setAttribute("class", "layer--hidden"))
        }
    }

    function i(e) {
        var t = $(".section_" + e),
            o = t.find("h1").text();
        f(),
            $("#current").html(o), t.addClass("active"), _.fadeIn(), $(".scene").slideUp(), setTimeout(function() { t.fadeIn("500", function() { l(e) }), f("refresh") }, 800), document.location.hash = e
    }

    function n(e) { $("body").attr("data-page", e); var t = C.indexOf(e); "contact" == k[t] ? $("footer").attr("data-function", "contact") : $("footer").attr("data-function", k[t]) }

    function s() { $(".section_portfolio .step2").removeClass("active"), $(".section_portfolio .step2 .nav .bt__info").removeClass("active"), $(".section_portfolio .step2 .info").fadeOut(), $(".section_portfolio .step2 video").each(function() { $(this)[0].pause() }), $(".section_portfolio .step1").addClass("active"), b = 0, header.className = "site--header visible", $(".hamburger").removeClass("active"), $(".contact").fadeOut(), $("#scenes").hide(), $("#jsRooms").fadeIn(), m.className = "", $("#footer").delay(300).fadeOut(), document.location.hash = "", setTimeout(function() { o() }, 300), $(".scene").fadeOut("500", function() { h.className += " active" }) }

    function a() {
        1 == $(".portfolio-slider").length && $(".portfolio-slider .slide").each(function() {
            var e = $(this).data("ratio");
            $(this).data("ratioinvert");
            $(window).width() > 768 && $(this).css({ width: $(".portfolio-slider").height() * e, height: $(".portfolio-slider").height() })
        })
    }

    function c(e) {
        var t, o, i, n, s = 0;
        $.grep(I, function(t, o) {-1 !== t.fileName.indexOf(e) && (s = o) }), t = 0 == s ? C.length - 1 : s - 1, o = s == C.length - 1 ? 0 : s + 1, i = x[t], n = x[o], $(".bx-prev").html("<span>" + i + "</span>"), $(".bx-next").html("<span>" + n + "</span>")
    }

    function r() { I.every(function(e) { return e.stop(), e }) }

    function l(e) { setTimeout(function() { $.grep(I, function(t, o) { if (-1 !== t.fileName.indexOf(e)) return t.play(), t }) }, 200) }

    function d() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 1;
        if (!$(".bx-wrapper").length) {
            var t = $(window).width();
            if (t >= 1024) var o = !1;
            else var o = !0;
            if (t < 1600 && t >= 1200);
            else if (t < 1200) { $(window).width() } else;
            if (t > 1700);
            y = $("#scenes .section__wrapper").bxSlider({
                mode: "fade",
                captions: !0,
                slideSelector: ".scene",
                pager: !1,
                adaptiveHeight: !0,
                keyboardEnabled: !0,
                startSlide: e,
                swipeThreshold: 100,
                nextText: "<span>Next</span>",
                prevText: "<span>Prev</span>",
                touchEnabled: o,
                onSliderLoad: function() { c($(".scene.active").attr("data-id")), $("#scenes").addClass("visible") },
                onSlideAfter: function(e) {
                    r(), $(".scene").removeClass("active"), e.addClass("active");
                    var t = e.find("h1").text();
                    $("#current").html(t), c($(".scene.active").attr("data-id")), l($(".scene.active").attr("data-id")), document.location.hash = $(".scene.active").attr("data-id"), n($(e).attr("data-id")), $(".section_portfolio .step1").addClass("active"), $(".section_portfolio .step2").removeClass("active"), $(".section_portfolio .step2 .nav .bt__info").removeClass("active"), $(".section_portfolio .step2 .info").fadeOut(), $(".section_portfolio .step2 video").each(function() { $(this)[0].pause() })
                }
            }), setTimeout(function() { y && y.reloadSlider() }, 400)
        }
    }

    function f(e) {
        /*  if ("refresh" == e) return $(".portfolio-slider").slick("setPosition"), !1;
         if (1 == $(".portfolio-slider").length && !$(".portfolio-slider").hasClass("init")) {
             var t = function(e, t) {
                 switch (t) {
                     case "init":
                         v = setInterval(function() { e[0].currentTime / e[0].duration * 100 >= 100 && ($(".section_portfolio .step2.active .work").removeClass("active"), $(".section_portfolio .step2.active .work__end").addClass("active"), clearInterval(v)) }, 100);
                         break;
                     case "destroy":
                         clearInterval(v)
                 }
             };
             $(".portfolio-slider").addClass("init"), $(".portfolio-slider").slick({ prevArrow: $(".slick-controls .slick-prev"), nextArrow: $(".slick-controls .slick-next"), dots: !1, infinite: !0, speed: 300, slidesToShow: 1, centerMode: !0, variableWidth: !0 }), $(document).on(p, ".section_portfolio  .step1 .portfolio-navigantion .categories a", function(e) {
                     var t = $(this).data("tag");
                     $(".section_portfolio  .step1 .portfolio-navigantion .categories a").removeClass("active"), $(this).addClass("active"), $(".portfolio-slider").slick("slickUnfilter"), $(".portfolio-slider").slick("slickFilter", "." + t), a()
                 }),

                 $(document).on(p, ".section_portfolio .step2 .nav .bt__close", function(e) { $(this).closest(".step2").removeClass("active"), $(this).closest(".step2").find(".nav .bt__info").removeClass("active"), $(this).closest(".step2").find(".info").fadeOut(), $(".section_portfolio .step1").addClass("active"), 1 == $(this).closest(".step2").find("video").length && ($(this).closest(".step2").find("video")[0].pause(), t("", "destroy")) }), $(document).on(p, ".section_portfolio .step2 .nav .bt__info", function(e) { $(this).toggleClass("active"), $(this).hasClass("active") ? $(this).closest(".step2").find(".info").fadeIn() : $(this).closest(".step2").find(".info").fadeOut() })
         } */
    }
    var v, p = "touchend" in document.documentElement ? "touchend" : "click",
        u = document.querySelectorAll(".layer--main"),
        h = (document.querySelectorAll(".layer--hidden"), document.getElementById("map"), document.getElementById("jsRooms")),
        m = document.getElementById("footer"),
        _ = $("#scenes"),
        g = document.getElementById("start"),
        w = document.getElementById("start2"),
        b = void 0,
        y = void 0,
        C = ["Services", "video_explainer", "Plant_list", "Clients", "Testimonials", "portfolio", "about"],
        x = ["Character&nbsp;design", "Video&nbsp;explainer", "Infographic", "Microanimation", "All&nbsp;in", "Portfolio", "About&nbsp;us", "Blogs", "contactus"],
        k = ["portfolio", "portfolio", "portfolio", "portfolio", "portfolio", "about", "portfolio", "Blogs"],
        I = [],
        E = $(window).innerWidth();
    if (E >= 1440) var O = ".json";
    else if (E < 1440 && E > 1024) var O = "_mid.json";
    else var O = "_min.json";
    if (function() {
            for (var e = 0; e < C.length; e++)
                if ("portfolio" != C[e] && "about" != C[e]) {
                    var t = document.getElementById("anim_" + C[e]),
                        o = "wp-content/themes/animat/scenes/" + C[e] + O,
                        i = { container: t, renderer: "svg", loop: !0, autoplay: !1, path: o },
                        n = bodymovin.loadAnimation(i);
                    I[e] = n
                }
        }(), function() {
            $(document).on("mouseenter", ".layer--active", function(e) {
                var t = $(this).data("id");
                $("#jsRooms .view #" + t).fadeIn();
            }), $(document).on("mouseleave", ".layer--active", function(e) { $("#jsRooms .view .layer--hidden").fadeOut(); }), $(document).on("click", ".layer--active", function(t) { e($(this).data("click")) });
            for (var t = 0; t < u.length; t++) u[t].addEventListener("click", function(t) { e(this.id) }), u[t].addEventListener("touchend", function(t) { e(this.id) }), u[t].addEventListener("mouseenter", function(e) {
                if (!$(".activeRoom").length) {
                    $(this).stop(!0, !0);
                    var t = this.id,
                        o = t + "_hover",
                        i = document.getElementById(o);
                    i && ($("body").hasClass("ie") || $("body").hasClass("firefox") ? $("#" + o).show() : $("#" + o).fadeIn(200), i.setAttribute("class", " layer-hidden activeRoom"))
                }
            }), u[t].addEventListener("mouseleave", function(e) { o() })
        }(), window.location.href.indexOf("#") > 0) {
        var R = location.hash,
            A = R.slice(R.indexOf("#") + 1);
        if (C.indexOf(A) > -1) {
            C.indexOf(A);
            e(A)
        }
    }
    g.addEventListener("click", function(e) { e.preventDefault(), s() }), w.addEventListener("click", function(e) { e.preventDefault(), s() }), $("#nav li a, #jsRooms .view_mobile a").on("click", function(e) {
        e.preventDefault();
        var o = $(this).data("id");
        b = 1, t(), d(), i(o), n(o)
    }), a(), $(window).on("resize", function() {
        a();
        var e = $(window).width();
        if (e < 1600 && e >= 1200);
        else if (e < 1200) { $(window).width() } else;
        if (e > 1700);
    })
}();