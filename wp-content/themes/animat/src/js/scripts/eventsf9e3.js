(function() {
    var enter = document.getElementById('enter');
    var follow = document.getElementById('follow');
    var startPos = document.getElementById('start');
    var fixed = document.getElementById('jsFixed');
    var header = document.getElementById('header');
    var hamburger = document.getElementById('hamburger');
    var popup_par = document.getElementById('popup');
    var popup = document.getElementById('popup__inner');
    var popup_parts = document.getElementById('popup__parts');
    var rooms = document.getElementById('jsRooms');
    var ctc = document.getElementById('ctc');
    var ctc_text = document.getElementById('ctc_text');
    var footer = document.getElementById('footer');
    var status;

    const path1 = document.getElementById('splash_1');
    const path2 = document.getElementById('splash_2');
    const path3 = document.getElementById('splash_3');
    const path4 = document.getElementById('splash_4');
    const path5 = document.getElementById('splash_5');
    const path6 = document.getElementById('splash_6');
    const path7 = document.getElementById('splash_7');
    const path8 = document.getElementById('splash_8');
    const path9 = document.getElementById('splash_9');

    const gooey_intro = new GooeyOverlay(path1, path2, path3);
    const gooey_contact = new GooeyOverlay(path4, path5, path6);

    setTimeout(function() {
        ctc.className += ' active';
    }, 800);

    if (window.location.href.indexOf("#") > -1) {
        header.className = 'site--header visible';
        $('#jsFixed').remove();
        // $('#jsRooms').fadeIn();
        if ($('#jsRooms').hasClass('active') === false) {
            rooms.className += ' active';
        }
        follow.style.opacity = '1';

    }



    //-------------------------------------------------------------------------------------------
    /*  setTimeout(function() {
        if (location.hash.substring(1, 9999).length == 0) {
            fixed.className += " hide";
            enter.className += " active";
            if (/Mobi/.test(navigator.userAgent) == false || $(window).width() >= 768) {
                cursor();
            }
            if (!$('body').hasClass('ie') && !$('body').hasClass('firefox')) {
                gooey_intro.open();
            } else {
                $('.splash_ie').addClass('active');
                setTimeout(function() {
                    $('.splash_ie').remove();
                }, 600);
            }
            whiteHeader();
            setTimeout(function() {
                header.className = 'site--header visible';
                if (!$('body').hasClass('ie') && !$('body').hasClass('firefox')) {
                    gooey_intro.close();
                }
                $('#jsRooms').fadeIn();
                if ($('#jsRooms').hasClass('active') === false) {
                    rooms.className += ' active';
                }

                follow.style.opacity = '1';
            }, 988);

            setTimeout(function() {
                $('#jsFixed').remove();
            }, 1000);
        }
    }, 500);
 */



    setTimeout(function() {
        if (location.hash.substring(1, 9999).length == 0) {
            fixed.className += " hide";
            enter.className += " active";

            if (/Mobi/.test(navigator.userAgent) == false || $(window).width() >= 768) {
                cursor();
            }

            whiteHeader();
            header.className = 'site--header visible';

            $('#jsRooms').fadeIn();
            if ($('#jsRooms').hasClass('active') === false) {
                rooms.className += ' active';
            }

            follow.style.opacity = '1';

            setTimeout(function() {
                $('#jsFixed').remove();
            }, 10);
        }
    }, 10);




    //-----------------------------------------------------------------------------------------------------
    /*  enter.addEventListener("click", function(event) {
         event.preventDefault();
         if (!status) {
             fixed.className += " hide";
             enter.className += " active";
             if (/Mobi/.test(navigator.userAgent) == false || $(window).width() >= 768) {
                 cursor();
             }
             if (!$('body').hasClass('ie') && !$('body').hasClass('firefox')) {
                 gooey_intro.open();
             } else {
                 $('.splash_ie').addClass('active');
                 setTimeout(function() {
                     $('.splash_ie').remove();
                 }, 600);
             }
             whiteHeader();
             setTimeout(function() {
                 header.className = 'site--header visible';
                 if (!$('body').hasClass('ie') && !$('body').hasClass('firefox')) {
                     gooey_intro.close();
                 }
                 $('#jsRooms').fadeIn();
                 if ($('#jsRooms').hasClass('active') === false) {
                     rooms.className += ' active';
                 }

                 follow.style.opacity = '1';
             }, 988);
             setTimeout(function() {
                 $('#jsFixed').remove();
             }, 1000);
         }
     }); */


    hamburger.addEventListener("click", function(event) {
        event.preventDefault();
        if ($('.hamburger').hasClass('active')) {
            if ($('body').attr('data-popup') == 1) {
                closeContact();
            } else {
                $('.contact').fadeOut();
                $('.footer #ctc_text').fadeOut();
                $('footer').removeClass('clicked');
                if ($(window).width() <= 768) {
                    $('footer').removeClass('actives');
                    // $('footer').delay(300).fadeOut();
                }
                $('body').attr('data-popup', '1');
                $('#ctc_text').removeClass('hidden');
                $('header').removeClass('color--white');
                $('.hamburger').removeClass('actives');
            }
        } else {
            popup_par.className += ' active';
            hamburger.className += ' active';
            if (!$('body').hasClass('ie') && !$('body').hasClass('firefox')) {
                gooey_contact.open();
            }
            $('.popup').show();
            setTimeout(function() {
                popup.className += ' visible';
            }, 300);
            setTimeout(function() {
                popup_parts.className += ' active';
            }, 700);
            if (!$('body').hasClass('ie') && !$('body').hasClass('firefox')) {
                setTimeout(function() {
                    gooey_contact.close();
                }, 1500);
            }
            whiteHeader();
        }
    });

    document.getElementById('start').addEventListener("click", function(event) {
        event.preventDefault();
        if ($('.popup').hasClass('active')) {
            $('header').removeClass('active');
            closeContact();
        }
    });
    document.getElementById('start2').addEventListener("click", function(event) {
        event.preventDefault();
        if ($('.popup').hasClass('active')) {
            $('header').removeClass('active');
            closeContact();
        }
    });








    $('.open__contact, footer').on('click', function(e) {

        function open__contact() {
            $('.contact').delay(300).fadeIn();
            $('body').attr('data-popup', '2');

            e.preventDefault();
            if (!$('body').hasClass('ie') && !$('body').hasClass('firefox')) {
                $('footer').addClass('clicked active');
            } else {
                $('.contact .bg__ie').fadeIn();
            }
            whiteHeader();
            if ($(window).width() <= 768) {
                footer.className = 'clicked active';
            }
            $('.hamburger').addClass('active');
            /* ctc_text.className += ' hidden'; */
            $('.contact').delay(300).fadeIn();
        }

        if ($(e.target).closest('footer').length == 1) {
            if ($('footer').attr('data-function') == 'contact') {
                open__contact();
            } else {
                $('#nav li a[data-id="' + $('footer').attr('data-function') + '"]').click()
            }
        } else {
            open__contact();
        }

    });

    $('#nav li a').on('click', function(e) {
        e.preventDefault();
        closeContact();
        $('#footer').fadeIn();
    })

    if (/Mobi/.test(navigator.userAgent) == false || $(window).width() >= 768) {
        cursor();
    }

    function cursor() {
        var divName = 'follow';
        var offX = 20;
        var offY = 20;

        function mouseX(evt) {
            if (!evt) evt = window.event;
            if (evt.pageX) return evt.pageX;
            else if (evt.clientX) return evt.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
            else return 0;
        }

        function mouseY(evt) {
            if (!evt) evt = window.event;
            if (evt.pageY) return evt.pageY;
            else if (evt.clientY) return evt.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
            else return 0;
        }

        function follow_div(evt) {
            var obj = document.getElementById(divName).style;
            obj.left = (parseInt(mouseX(evt)) + offX) + 'px';
            obj.top = (parseInt(mouseY(evt)) + offY) + 'px';
        }
        document.onmousemove = follow_div;
    }

    function whiteHeader() {
        header.className += ' color--white visible';
    }

    function closeContact() {
        popup.className = 'flex';
        setTimeout(function() {
            popup_parts.className = 'popup__parts';
        }, 500);
        setTimeout(function() {
            popup_par.className = 'popup flex flex--hcenter';
        }, 400);
        hamburger.className = 'hamburger visible';

        if (!$('body').hasClass('ie') && !$('body').hasClass('firefox')) {
            setTimeout(function() {
                gooey_contact.close();
            }, 500);
        }

        setTimeout(function() {
            $('.popup').hide();
        }, 1000);
        if (!$('header').hasClass('visible')) {
            header.className = 'site--header active visible';
        } else {
            header.className = 'site--header active visible';
        }
        $('.hamburger').removeClass('active');

    }
})();